@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">AGREGAR CLIENTE</div>

                <div class="col text-right">
                    <a href="{{ route('register.client')}}" class="btn btn-sm btm-success">CANCELAR</a>
                </div>

                <div class="card-body">
                    <form role="form" method="post" action="{{ route('add.client')}}">
                        

                        <div class="row">  
                            <div class="col-lg-4">
                            <label class="from-control-label" for="nombre">Nombre Del Cliente</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>

 
                            <div class="col-lg-4">
                            <label class="from-control-label" for="apellidos">Apellidos Del Cliente</label>
                            <input type="text" class="from-control" name="apellidos">
                        </div>
                        
                         
                            <div class="col-lg-4">
                            <label class="from-control-label" for="cedula">Cedula Del Cliente</label>
                            <input type="text" class="from-control" name="cedula">
                        </div>
                        
                          
                            <div class="col-lg-4">
                            <label class="from-control-label" for="dirección">Dirección Del Cliente</label>
                            <input type="text" class="from-control" name="dirección">
                        </div>

                         
                            <div class="col-lg-4">
                            <label class="from-control-label" for="teléfono">Telefono Celular</label>
                            <input type="text" class="from-control" name="teléfono">
                        </div>

                         
                            <div class="col-lg-4">
                            <label class="from-control-label" for="fecha_nacimiento">Fecha De Nacimiento</label>
                            <input type="text" class="from-control" name="fecha_nacimiento">
                        </div>

                            <div class="col-lg-4">
                            <label class="from-control-label" for="email">Email Del Cliente</label>
                            <input type="text" class="from-control" name="email">
                        </div>

                        </div>

                        <button type="submit" class="btn btn-succes pull-right">Guardar</button>

                    </form>
                
                    
                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
