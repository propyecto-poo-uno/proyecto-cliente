<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function(){

    
    Route::get('/registro/cliente', ['as' => 'register.client', 'uses' =>'ClienteController@RegistroCliente']);
    Route::get('/registro/agregar', ['as' => 'add.client', 'uses' =>'ClienteController@DatosCliente']);
    Route::post('/guardar/cliente/prueba', ['as' => 'save.client', 'uses' =>'ClienteController@GuardarCliente']);
    
});

/*
Route::group(['middleware' => ['auth']], function(){

    
    Route::get('/lista/productos', ['as' => 'list.productos', 'uses' =>'ProductoController@InicioProducto']);
    Route::get('/crear/productos', ['as' => 'crear.productos', 'uses' =>'ProductoController@CrearProducto']);
    Route::post('/guardar/productos/prueba', ['as' => 'productos.guardar', 'uses' =>'ProductoController@GuardarProducto']);
    


});
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
