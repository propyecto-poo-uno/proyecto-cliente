<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function RegistroCliente(Request $request)
    {
        //dd('Hola mundo');
        $cliente = Cliente::all();
        
        //dd($cliente);

        return view('productos.llegada')->with('cliente', $cliente);
        
    }

    public function DatosCliente(Request $request)
    {
        $cliente = Cliente::all();

        return view('productos.agregar')->with('cliente', $cliente);

    }
    
    public function GuardarCliente(Request $request)
    {        
        $this->validate($request, [

            'nombre' => 'required',
            'apellidos' => 'required',
            'cedula' => 'required',
            'dirección' => 'required',
            'teléfono' => 'required',
            'fecha_nacimiento' => 'required',
            'email' => 'required'

        ]);

        $cliente = new Cliente();
        $cliente->nombre            = $request->nombre;
        $cliente->apellidos         = $request->apellidos;
        $cliente->cedula            = $request->cedula;
        $cliente->dirección         = $request->dirección;
        $cliente->teléfono          = $request->teléfono;
        $cliente->fecha_nacimiento  = $request->fecha_nacimiento;
        $cliente->email	            = $request->email;
        $cliente->save();
        return redirect()->route('register.client');


    }

}